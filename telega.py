# import os
# import subprocess
# import time
# from traceback import format_exc
# import unittest
# import requests
#
# from appium import webdriver
# from appium.options.android import UiAutomator2Options
# from appium.webdriver.common.appiumby import AppiumBy
# from selenium.webdriver.support import expected_conditions as EC
# from selenium.webdriver.support.ui import WebDriverWait
#
#
# appium_server_url = 'http://localhost:4723/wd/hub'
#
#
# class SetProxy():
#     def __init__(self, device_name: str) -> None:
#         """
#         Метод для установки прокси на устройстве. Детали прокси жестко заданы согласно требованиям.
#         """
#         self.device_name = device_name
#         self.capabilities = dict(
#             platformName='Android',
#             automationName='uiautomator2',
#             udid='emulator-5554',
#             appPackage='io.nekohasekai.sagernet',
#             appActivity='io.nekohasekai.sagernet.ui.MainActivity',
#             language='en',
#             locale='US',
#             noReset=True,
#         )
#
#         # Жестко заданные настройки прокси
#         self.proxy = "154.195.158.205:64137:JBkGJrsh:ZKs36vre"
#         change_proxy = self.proxy.split(":")
#         if len(change_proxy) == 4:
#             self.host = change_proxy[0]
#             self.port = int(change_proxy[1])
#             self.username = change_proxy[2]
#             self.password = change_proxy[3]
#             print(f'Настройки прокси заданы для {self.device_name}')
#         else:
#             raise ValueError(f"Неправильный формат прокси {self.proxy}, формат должен быть ip:port:login:password")
#
#     def run(self) -> bool:
#         self.setUp()
#         res = self.add_proxy()
#         print('Прокси добавлен' if res else 'Прокси не добавлен...')
#         res = self.on_proxy()
#         print('Прокси активен' if res else 'Прокси не активен...')
#         return res
#
#     def setUp(self) -> None:
#         self.driver = webdriver.Remote(appium_server_url,
#                                        options=UiAutomator2Options().load_capabilities(self.capabilities))
#         print("SagerNet запущен")
#
#     def tearDown(self) -> None:
#         if self.driver:
#             self.driver.quit()
#
#     def add_proxy(self) -> bool:
#         print('Adding proxy')
#         try:
#             # clicks on add btn
#             add_btn = WebDriverWait(self.driver, 2).until(EC.presence_of_element_located(
#                 (AppiumBy.XPATH, '//android.widget.TextView[@content-desc="Добавить профиль"]')))
#             add_btn.click()
#             # time.sleep(2)
#             print('Clicked on Add Profile')
#
#             # clicks on manual add btn
#             manual_btn = WebDriverWait(self.driver, 2).until(EC.presence_of_element_located(
#                 (AppiumBy.XPATH, '//android.widget.ImageView[@resource-id="io.nekohasekai.sagernet:id/submenuarrow"]')))
#             manual_btn.click()
#             # time.sleep(2)
#             print('Clicked on selector')
#
#             # selects SOCKS
#             socks_btn = WebDriverWait(self.driver, 2).until(EC.presence_of_element_located((AppiumBy.XPATH,
#                                                                                             '//android.widget.TextView[@resource-id="io.nekohasekai.sagernet:id/title" and @text="SOCKS"]')))
#             socks_btn.click()
#             # time.sleep(2)
#             print('Clicked on SOCKS')
#
#             # Setting server
#             server_btn = WebDriverWait(self.driver, 2).until(EC.presence_of_element_located((AppiumBy.XPATH,
#                                                                                              '//androidx.recyclerview.widget.RecyclerView[@resource-id="io.nekohasekai.sagernet:id/recycler_view"]/android.widget.LinearLayout[3]')))
#             server_btn.click()
#             print('Clicked on server btn')
#             # time.sleep(2)
#             server_input = WebDriverWait(self.driver, 2).until(EC.presence_of_element_located(
#                 (AppiumBy.XPATH, '//android.widget.EditText[@resource-id="android:id/edit"]')))
#             server_input.clear()
#             print('Cleared server input')
#             # time.sleep(1)
#             server_input.send_keys(self.host)
#             print(f'Sended {self.host} to server input')
#             # time.sleep(2)
#             try:
#                 server_ok = WebDriverWait(self.driver, 2).until(EC.presence_of_element_located(
#                     (AppiumBy.XPATH, '//android.widget.Button[@resource-id="android:id/button1"]')))
#                 server_ok.click()
#                 print(f'Clicked server OK')
#             except:
#                 print("no OK")
#             # time.sleep(2)
#
#             # Setting port
#             port_btn = WebDriverWait(self.driver, 2).until(EC.presence_of_element_located((AppiumBy.XPATH,
#                                                                                            '//androidx.recyclerview.widget.RecyclerView[@resource-id="io.nekohasekai.sagernet:id/recycler_view"]/android.widget.LinearLayout[4]')))
#             port_btn.click()
#             print('Clicked on port btn')
#             # time.sleep(2)
#             port_input = WebDriverWait(self.driver, 2).until(EC.presence_of_element_located(
#                 (AppiumBy.XPATH, '//android.widget.EditText[@resource-id="android:id/edit"]')))
#             port_input.clear()
#             print('Cleared port input')
#             # time.sleep(1)
#             port_input.send_keys(self.port)
#             print(f'Sended {self.port} to port input')
#             # time.sleep(2)
#             port_ok = WebDriverWait(self.driver, 2).until(EC.presence_of_element_located(
#                 (AppiumBy.XPATH, '//android.widget.Button[@resource-id="android:id/button1"]')))
#             port_ok.click()
#             print(f'Clicked port OK')
#             # time.sleep(2)
#
#             if self.username and self.password:
#                 # Setting username
#                 username_btn = WebDriverWait(self.driver, 2).until(EC.presence_of_element_located((AppiumBy.XPATH,
#                                                                                                    '//androidx.recyclerview.widget.RecyclerView[@resource-id="io.nekohasekai.sagernet:id/recycler_view"]/android.widget.LinearLayout[5]')))
#                 username_btn.click()
#                 print('Clicked on username btn')
#                 # time.sleep(2)
#                 username_input = WebDriverWait(self.driver, 2).until(EC.presence_of_element_located(
#                     (AppiumBy.XPATH, '//android.widget.EditText[@resource-id="android:id/edit"]')))
#                 username_input.clear()
#                 print('Cleared username input')
#                 # time.sleep(1)
#                 username_input.send_keys(self.username)
#                 print(f'Sended {self.username} to username input')
#                 # time.sleep(2)
#                 username_ok = WebDriverWait(self.driver, 2).until(EC.presence_of_element_located(
#                     (AppiumBy.XPATH, '//android.widget.Button[@resource-id="android:id/button1"]')))
#                 username_ok.click()
#                 print(f'Clicked username OK')
#                 # time.sleep(2)
#
#                 # Setting password
#                 password_btn = WebDriverWait(self.driver, 2).until(EC.presence_of_element_located((AppiumBy.XPATH,
#                                                                                                    '//androidx.recyclerview.widget.RecyclerView[@resource-id="io.nekohasekai.sagernet:id/recycler_view"]/android.widget.LinearLayout[6]')))
#                 password_btn.click()
#                 print('Clicked on password btn')
#                 # time.sleep(2)
#                 password_input = WebDriverWait(self.driver, 2).until(EC.presence_of_element_located(
#                     (AppiumBy.XPATH, '//android.widget.EditText[@resource-id="android:id/edit"]')))
#                 password_input.clear()
#                 print('Cleared password input')
#                 # time.sleep(1)
#                 password_input.send_keys(self.password)
#                 print(f'Sended {self.password} to password input')
#                 # time.sleep(2)
#                 password_ok = WebDriverWait(self.driver, 2).until(EC.presence_of_element_located(
#                     (AppiumBy.XPATH, '//android.widget.Button[@resource-id="android:id/button1"]')))
#                 password_ok.click()
#                 print(f'Clicked password OK')
#                 # time.sleep(2)
#
#             apply_btn = WebDriverWait(self.driver, 2).until(
#                 EC.presence_of_element_located((AppiumBy.XPATH, '//android.widget.TextView[@content-desc="Применить"]')))
#             apply_btn.click()
#             print(f'Clicked apply OK')
#             return True
#         except:
#             print(format_exc())
#             return False
#
#     def on_proxy(self) -> bool:
#         try:
#             proxy_tab = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((AppiumBy.XPATH,
#                                                                                              '//android.widget.LinearLayout[@resource-id="io.nekohasekai.sagernet:id/content_lin"]')))
#             proxy_tab.click()
#             time.sleep(5)
#             connect_btn = WebDriverWait(self.driver, 2).until(EC.presence_of_element_located(
#                 (AppiumBy.XPATH, '//android.widget.ImageButton[@content-desc="Подключиться"]')))
#             connect_btn.click()
#             try:
#                 ok_btn = WebDriverWait(self.driver, 3).until(EC.presence_of_element_located(
#                     (AppiumBy.XPATH, '//android.widget.Button[@resource-id="android:id/button1"]')))
#                 ok_btn.click()
#                 connect_btn = WebDriverWait(self.driver, 2).until(EC.presence_of_element_located(
#                     (AppiumBy.XPATH, '//android.widget.ImageButton[@content-desc="Подключиться"]')))
#                 connect_btn.click()
#             except:
#                 print("no Alert")
#             time.sleep(5)
#             return True
#
#         except:
#             print(format_exc())
#             return False
#
#
#
# if __name__ == "__main__":
#     # Предположим, что device_name - это уникальный идентификатор вашего устройства (UDID).
#     # Вы должны заменить 'your_device_udid' на актуальный UDID вашего устройства Android.
#     device_name = "emulator-5554"
#
#     # Создаем экземпляр класса SetProxy
#     proxy_setter = SetProxy(device_name=device_name)
#
#     # Запускаем процесс добавления прокси
#     result = proxy_setter.run()
#
#     # Выводим результат выполнения
#     if result:
#         print("Прокси успешно настроен и активирован.")
#     else:
#         print("В процессе настройки прокси произошла ошибка.")
#
#     # Не забываем корректно завершить сессию с драйвером
#     proxy_setter.tearDown()
from appium import webdriver
from appium.options.android import UiAutomator2Options
from appium.webdriver.common.appiumby import AppiumBy
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
import time

class Telegramm():
    def __init__(self, device_name: str) -> None:
        """
        Метод для запуска Telegram на устройстве.
        """
        self.device_name = device_name
        self.capabilities = dict(
            platformName='Android',
            automationName='uiautomator2',
            udid=device_name,
            appPackage='org.telegram.messenger.web',
            appActivity='org.telegram.messenger.DefaultIcon',
            language='en',
            locale='US',
            noReset=True,
        )
        self.driver = None

    def setUp(self):
        """
        Настройка и запуск драйвера Appium.
        """
        appium_server_url = 'http://localhost:4723/wd/hub'

        self.driver = webdriver.Remote(appium_server_url,
                                       options=UiAutomator2Options().load_capabilities(self.capabilities))

        print("Telegram запущен")

    def telegram_reg(self):
        """
        Функция для регистрации в Telegram.
        """
        print("Начало регистрации в Telegram")
        try:

            self.setUp()

            start_messaging_btn = self.driver.find_element(AppiumBy.XPATH, '//android.widget.TextView[@text="Start Messaging"]')

            start_messaging_btn.click()

            element_locator = (AppiumBy.XPATH, '//android.view.ViewGroup/android.widget.ImageView')

            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(element_locator)
            )

            for _ in range(12):
                element.click()

            phone_number = input("Введите ваш номер телефона: ")

            digits = list(phone_number)

            for digit in digits:
                position = int(digit)
                button_xpath = f"//android.view.ViewGroup/android.view.View[{position}]"
                button = WebDriverWait(self.driver, 10).until(
                    EC.element_to_be_clickable((AppiumBy.XPATH, button_xpath))
                )
                button.click()

            continue_button_xpath = '//android.widget.FrameLayout[@content-desc="Done"]/android.view.View'

            continue_button = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((AppiumBy.XPATH, continue_button_xpath))
            )

            continue_button.click()
            kod_number = input("Введите ваш код: ")
            kod_digits = list(kod_number)

            for kod_digit in kod_digits:
                position = int(kod_digit)
                button_xpath = f"//android.view.ViewGroup/android.view.View[{position}]"
                button = WebDriverWait(self.driver, 10).until(
                    EC.element_to_be_clickable((AppiumBy.XPATH, button_xpath))
                )
                button.click()
            print("Регистрация завершена. Пожалуйста, завершите проверку через приложение.")
            return True
        except Exception as e:
            print(f"Произошла ошибка при регистрации в Telegram: {e}")
            return False
        finally:
            self.tearDown()

    def tearDown(self):
        """
        Завершение работы драйвера и закрытие сессии.
        """
        if self.driver:
            self.driver.quit()
            print("Сессия завершена.")

# Пример использования
if __name__ == "__main__":
    device_name = "emulator-5554"  # Замените на ваше значение
    telegram = Telegramm(device_name)
    telegram.telegram_reg()
