from appium import webdriver
from appium.options.android import UiAutomator2Options
from appium.webdriver.common.appiumby import AppiumBy
import base64
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.actions.action_builder import ActionBuilder
from selenium.webdriver.common.actions.pointer_input import PointerInput
from selenium.webdriver.common.actions import interaction
import time
import json


class WhatsApp():
    def __init__(self, device_name: str) -> None:
        """
        Метод для запуска Telegram на устройстве.
        """
        with open("session_data.json", 'r') as f:
            session_data = json.load(f)

        # Устанавливаем активность, сохраненную в файле

        self.device_name = device_name
        self.capabilities = dict(
            platformName='Android',
            automationName='uiautomator2',
            udid=device_name,
            appPackage='com.whatsapp',
            appActivity='com.whatsapp.Main',
            language='en',
            locale='US',
            noReset=True,
            fullReset=False,
            # platformName='Android',
            # automationName='uiautomator2', # Укажите версию Android
            # udid=device_name,  # Имя устройства или 'Android Emulator'
            # noReset=True,
        )
        self.capabilities["appActivity"] = session_data['activity']
        print(self.capabilities["appActivity"])
        self.driver = None


    def setUp(self):
        """
        Настройка и запуск драйвера Appium.
        """
        appium_server_url = 'http://localhost:4723/wd/hub'

        self.driver = webdriver.Remote(appium_server_url,
                                       options=UiAutomator2Options().load_capabilities(self.capabilities))
        activity_data = self.driver.current_activity  # Исправлено здесь
        print("Текущая активность:", activity_data)
        with open("session_data.json", 'w') as f:
            json.dump({'activity': activity_data}, f)
        print("WhatsApp запущен")

    def get_whatsapp_chats(self):
        self.setUp()
        time.sleep(2)
        first_chat_element = self.driver.find_element(AppiumBy.XPATH,
                                                      "(//android.widget.RelativeLayout[@resource-id='com.whatsapp:id/contact_row_container'])[1]")
        time.sleep(2)  # пауза для стабилизации интерфейса

        # Поиск элемента с именем внутри контейнера чата
        chat_name = first_chat_element.find_element(AppiumBy.ID, 'com.whatsapp:id/conversations_row_contact_name').text

        print(f"Имя первого чата: {chat_name}")
        third_chat_element = self.driver.find_element(AppiumBy.XPATH,
                                                      "(//android.widget.LinearLayout[@resource-id='com.whatsapp:id/conversations_row_content'])[3]")
        time.sleep(2)  # пауза для стабилизации интерфейса

        # Получение имени чата
        chat_name = third_chat_element.find_element(AppiumBy.ID, 'com.whatsapp:id/conversations_row_contact_name').text

        # Попытка найти элемент с количеством непрочитанных сообщений
        try:
            new_messages_count = third_chat_element.find_element(AppiumBy.ID,
                                                                 'com.whatsapp:id/conversations_row_message_count').text
            new_messages_count = int(new_messages_count)  # Преобразуем строку в число
        except:
            new_messages_count = 0  # Если элемент не найден, считаем, что непрочитанных сообщений нет

        print(f"Имя чата: {chat_name}, Непрочитанные сообщения: {new_messages_count}")
        # Итерация по элементам, представляющим чаты

    def contact_add_whatsapp(self):
        """
        Функция для добавления контакта в WhatsApp.
        """
        print("Начало регистрации в WhatsApp")
        try:

            self.setUp()

            start_messaging_btn = (self.driver.find_element(
                AppiumBy.XPATH, '//android.widget.Button[@content-desc="Отправить сообщение"]'))
            start_messaging_btn.click()
            new_contact_btn = self.driver.find_element(
                AppiumBy.XPATH, '(//android.widget.LinearLayout[@resource-id="com.whatsapp:id/contactpicker_text_container"])[2]/android.widget.LinearLayout/android.widget.FrameLayout')
            new_contact_btn.click()
            time.sleep(2)
            print(0)
            name_add = self.driver.find_element(
                AppiumBy.XPATH, '//android.widget.EditText[@resource-id="com.whatsapp:id/first_name_field"]')
            time.sleep(2)
            print(1)
            name_add.send_keys("Влад")
            time.sleep(1)
            print(2)
            surname_add = self.driver.find_element(
                AppiumBy.XPATH, '//android.widget.EditText[@resource-id="com.whatsapp:id/last_name_field"]')
            time.sleep(1)
            surname_add.send_keys("Бугровый")
            time.sleep(1)
            country_cod = self.driver.find_element(
                AppiumBy.XPATH, '//android.widget.EditText[@resource-id="com.whatsapp:id/country_code_field"]')
            time.sleep(1)
            country_cod.click()
            time.sleep(1)
            rus_number = self.driver.find_element(
                AppiumBy.XPATH, '//android.widget.ListView[@resource-id="android:id/list"]/android.widget.LinearLayout[2]/android.widget.LinearLayout')
            time.sleep(1)
            rus_number.click()
            time.sleep(1)
            number_add = self.driver.find_element(
                AppiumBy.XPATH, '//android.widget.EditText[@resource-id="com.whatsapp:id/phone_field"]')
            time.sleep(1)
            number_add.send_keys("9788342584")
            time.sleep(1)
            save_btn = self.driver.find_element(
                AppiumBy.XPATH, '//android.widget.Button[@resource-id="com.whatsapp:id/keyboard_aware_save_button"]')
            time.sleep(1)
            save_btn.click()
        except Exception:
            print("В процессе регистрации произошла ошибка")

    def set_whatsapp_profile_picture(self):
        # Открыть меню настроек WhatsApp
        self.setUp()
        more = (self.driver.find_element(
            AppiumBy.XPATH, '//android.widget.ImageView[@content-desc="Ещё"]'))
        more.click()
        time.sleep(1)
        settings = (self.driver.find_element(
            AppiumBy.XPATH, '//android.widget.TextView[@resource-id="com.whatsapp:id/title" and @text="Настройки"]'))
        settings.click()
        time.sleep(1)
        profile = (self.driver.find_element(
            AppiumBy.ID, 'com.whatsapp:id/profile_info_photo'))
        profile.click()
        time.sleep(1)
        edit_photo = (self.driver.find_element(
            AppiumBy.ID, 'com.whatsapp:id/change_photo_btn'))
        edit_photo.click()
        time.sleep(1)
        gallery = (self.driver.find_element(
            AppiumBy.XPATH, '//androidx.recyclerview.widget.RecyclerView[@resource-id="com.whatsapp:id/intent_recycler"]/android.widget.Button[2]'))
        gallery.click()
        time.sleep(1)
        new_photo = (self.driver.find_element(
            AppiumBy.XPATH, '(//android.widget.ImageView[@resource-id="com.whatsapp:id/thumbnail"])[1]'))
        new_photo.click()
        time.sleep(1)
        photo_add = (self.driver.find_element(
            AppiumBy.XPATH, '//android.widget.ImageView[@content-desc="Фото"]'))
        photo_add.click()
        time.sleep(1)
        photo_ready = (self.driver.find_element(
            AppiumBy.XPATH, '//android.widget.Button[@resource-id="com.whatsapp:id/ok_btn"]'))
        photo_ready.click()

    def get_multiple_chats_info(self, max_chats=10):
        self.setUp()
        time.sleep(2)  # даем время приложению загрузиться

        # Прокрутка и поиск чатов
        chats = []
        chats_xpath = "//android.widget.LinearLayout[@resource-id='com.whatsapp:id/conversations_row_content']"
        found_chats = self.driver.find_elements(AppiumBy.XPATH, chats_xpath)
        print(f'Found {len(found_chats)} chats')
        while len(chats) < max_chats and found_chats:
            for chat_element in found_chats:
                if len(chats) >= max_chats:
                    break

                # Получение имени чата
                try:
                    chat_name = chat_element.find_element(AppiumBy.ID,
                                                          'com.whatsapp:id/conversations_row_contact_name').text
                except:
                    chat_name = "Name not found"

                try:
                    new_messages_count = chat_element.find_element(AppiumBy.ID,
                                                                   'com.whatsapp:id/conversations_row_message_count').text
                    new_messages_count = int(new_messages_count)
                except:
                    new_messages_count = 0

                chats.append({"name": chat_name, "new_messages": new_messages_count})

            #Прокрутка списка вниз
            if len(chats) < max_chats:
                self.scroll_down()

            found_chats = self.driver.find_elements(AppiumBy.XPATH, chats_xpath)
            print(f'Found {len(found_chats)} chats')
        self.driver.quit()  # закрываем драйвер после выполнения теста
        return self.remove_duplicates(chats)

    def scroll_down(self):
        """Прокрутка списка чатов вниз с использованием W3C Action."""
        action_builder = ActionBuilder(self.driver, mouse=PointerInput(interaction.POINTER_TOUCH, "touch"))
        # начальные и конечные координаты x и y для скролла
        start_x = 500
        start_y = 1600
        end_x = 500
        end_y = 300

        # добавляем действия скролла
        action_builder.pointer_action.move_to_location(start_x, start_y)
        action_builder.pointer_action.pointer_down()
        action_builder.pointer_action.pause(0.5)  # пауза для "захвата" экрана
        action_builder.pointer_action.move_to_location(end_x, end_y)
        action_builder.pointer_action.release()

        # создаём экземпляр ActionChains и заменяем стандартные действия на созданные
        actions = ActionChains(self.driver)
        actions.w3c_actions = action_builder

        # выполняем скролл
        actions.perform()

        time.sleep(2)  # даем время для обновления элементов списка

    def remove_duplicates(self, chats):
        unique_chats = {}
        for chat in chats:
            if chat['name'] not in unique_chats:
                unique_chats[chat['name']] = chat
        return list(unique_chats.values())

    def send_text_message(self, chat_name, message_text):
        self.setUp()
        chat_element = self.driver.find_element(AppiumBy.ANDROID_UIAUTOMATOR,
                                           'new UiSelector().text("{}")'.format(chat_name))
        chat_element.click()
        time.sleep(1)
        # Находим поле ввода и вводим текст сообщения
        input_field = self.driver.find_element(AppiumBy.ID, 'com.whatsapp:id/entry')
        input_field.send_keys(message_text)
        time.sleep(1)
        # Нажимаем кнопку отправки сообщения
        send_button = self.driver.find_element(AppiumBy.ID, 'com.whatsapp:id/send')
        send_button.click()

    def send_image(self, chat_name, image_path):
        self.setUp()
        file_path = image_path  # Путь к файлу на компьютере
        android_path = '/storage/emulated/0/Pictures/photo_1.jpg'  # Путь на устройстве
        with open(file_path, "rb") as image_file:
            encoded_string = base64.b64encode(image_file.read()).decode('utf-8')
        self.driver.push_file(android_path, encoded_string)
        chat_element = self.driver.find_element(AppiumBy.ANDROID_UIAUTOMATOR,
                                                'new UiSelector().text("{}")'.format(chat_name))
        chat_element.click()
        time.sleep(1)
        attach = self.driver.find_element(AppiumBy.ID, "com.whatsapp:id/input_attach_button")
        attach.click()
        time.sleep(1)
        gallery = self.driver.find_element(AppiumBy.ID, "com.whatsapp:id/pickfiletype_gallery_holder")
        gallery.click()
        time.sleep(1)
        pictures = self.driver.find_element(AppiumBy.XPATH, '//android.widget.TextView[@resource-id="com.whatsapp:id/title" and @text="Pictures"]')
        pictures.click()
        time.sleep(1)
        print(1)
        photo = self.driver.find_element(AppiumBy.XPATH, '//android.widget.ImageView[@content-desc="Фото"]')
        print(2)
        photo.click()
        time.sleep(1)
        print(3)
        send_button = self.driver.find_element(AppiumBy.ID, 'com.whatsapp:id/send')
        send_button.click()
        print(4)
        time.sleep(2)



if __name__ == "__main__":
    device_name = "emulator-5554"  # Замените на ваше значение
    telegram = WhatsApp(device_name)
    telegram.setUp()
    #telegram.contact_add_whatsapp()
    #telegram.get_whatsapp_chats()
    #telegram.set_whatsapp_profile_picture()
    #telegram.send_text_message("На", 'Привет!')
    #telegram.send_image("В В", "C:/Users/Admin/Desktop/7bb965750a8b043509caf7ec2e3ce8b9.jpg")
    #chats_info = telegram.get_multiple_chats_info(max_chats=15)
    #print("Chats info:", chats_info)
    #for chat in chats_info:
     #   print(chat)
